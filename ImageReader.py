import PIL.ImageGrab
# PIL.ImageGrab.grab().load()[x,y] for pixel
# PIL.ImageGrab.grab().crop((470,440,787,576)) to get game screen


def get_pixels():
	all = []
	img = get_game_screen()
	for j in range(0,img.height):
		for i in range(0, img.width):
			all += [[((img.getpixel((i,j))[1])/float(255))]]
	return all[::10]



def get_game_screen():
	return PIL.ImageGrab.grab().crop((470,440,787,576))

