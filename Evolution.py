import NN
from ImageReader import *
from Keyboard import Press

input_len = 4312

class Creature:
	def __init__(self):
		self.ai = NN.NN([input_len,input_len,4])

	def press(self):
		ans = self.ai.feedforward(get_pixels())
		max_index = ans.index(max(ans))
		key = {0:'q',1:'w',2:'o',3:'p'}[max_index]
		Press(key)

crtrs = [ Creature() ]* 15
