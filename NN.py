import random as r
import numpy as np


class NN:
	# [20, 20, 20, 3]
	def __init__(self, sizes):
		self.num_layers = len(sizes)
		self.sizes = sizes
		self.biases = [np.random.randn(y, 1) for y in sizes[1:]]
		self.weights = [np.random.randn(y, x) for x, y in zip(sizes[:-1], sizes[1:])]
		self.lastSave = False

	'''
	[ [x] *sizes ]
	example: [ [0], [0.5] ]
	'''
	def feedforward(self, a):
		a = to_array(a)
		for b, w in zip(self.biases, self.weights):
			a = sigmoid(np.dot(w, a)+b)
		a = to_list(a)
		return a

	def fit(self):
		times = 50   # amount of changes
		for i in range(times):
			if r.randint(0,1) == 1:
				#  random indexes
				x = r.randint(0,len(self.weights)-1)
				y = r.randint(0,len(self.weights[x])-1)
				z = r.randint(0,len(self.weights[x][y])-1)
				self.weights[x][y][z] = r.randint(-2,2) + r.random()
			else:
				#  random indexes
				x = r.randint(0,len(self.biases)-1)
				y = r.randint(0,len(self.biases[x])-1)
				z = r.randint(0,len(self.biases[x][y])-1)
				self.biases[x][y][z] = r.randint(-2,2) + r.random()
		self.lastSave = self.getCopy()

	def revert(self):
		if self.lastSave:
			self.load(self.lastSave)

	def getCopy(self):
		return [self.num_layers, self.sizes[:], to_list(self.biases), to_list(self.weights)]

	def load(self, ann):
		self.num_layers, self.sizes, self.biases, self.weights = ann
		self.biases = to_array(self.biases)
		self.weights = to_array(self.weights)

def to_list(l):  # PYTHON
	# list of numpy arrays to a list of lists
	if type(l) == np.ndarray:
		return list((x.tolist() for x in l))
	else: return l

def to_array(l):  # NUMPY
	# list of lists to a list of numpy arrays
	if type(l) == list:
		return list((np.array(x) for x in l))
	else:
		return l


def sigmoid(z):
	return 1.0/(1.0+np.exp(-z))

def shuffled(l):
	l = list(l)
	r.shuffle(l)
	return l

def get_in_ranges(l):
	#  returns all combinations of indexes in l
	ans = []
	for i in range(len(l)):
		for j in range(len(l[i])):
			ans.append((i,j))
	return ans

